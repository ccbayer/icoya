'use strict';

/**
 * @ngdoc function
 * @name icoYaNgApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the icoYaNgApp
 */
angular.module('icoYaNgApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
