'use strict';

/**
 * @ngdoc function
 * @name icoYaNgApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the icoYaNgApp
 */
angular.module('icoYaNgApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
